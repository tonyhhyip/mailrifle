package sender

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"gocloud.dev/blob"
)

type AttachmentLoader interface {
	Load(ctx context.Context, url string) (io.ReadCloser, error)
}

type HttpClientAttachmentLoader struct {
	Client *http.Client
}

func (l *HttpClientAttachmentLoader) Load(ctx context.Context, url string) (io.ReadCloser, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	res, err := l.Client.Do(req.WithContext(ctx))
	if err != nil {
		return nil, err
	}
	return res.Body, nil
}

type GoCloudBlobAttachmentLoader struct {
	HttpClientAttachmentLoader
}

func (l *GoCloudBlobAttachmentLoader) Load(ctx context.Context, uri string) (_ io.ReadCloser, err error) {
	u, err := url.Parse(uri)
	if err != nil {
		return
	}
	if u.Scheme == "http" || u.Scheme == "https" {
		return l.HttpClientAttachmentLoader.Load(ctx, uri)
	}

	bucket, err := blob.OpenBucket(ctx, fmt.Sprintf("%s://%s?%s", u.Scheme, u.Host, u.RawQuery))
	if err != nil {
		return
	}
	defer func() {
		_ = bucket.Close()
	}()

	return bucket.NewReader(ctx, u.Path[1:], nil)
}
