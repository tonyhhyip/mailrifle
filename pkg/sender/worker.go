package sender

import (
	"bytes"
	"context"
	"io"
	"net/smtp"
	"sync"

	"github.com/domodwyer/mailyak"
	"gitlab.com/tonyhhyip/mailrifle/pkg/api"
)

type buffer interface {
	io.ReadWriter
	Reset()
	Bytes() []byte
}

func NewWorker() *Worker {
	return &Worker{
		BufferPool: &sync.Pool{
			New: func() interface{} {
				return new(bytes.Buffer)
			},
		},
	}
}

type Worker struct {
	Addr       string
	Auth       smtp.Auth
	BufferPool *sync.Pool
	Loader     AttachmentLoader
	Signer     DKIMSigner
}

func (w *Worker) Process(ctx context.Context, mail *api.Mail) (err error) {
	email, cleanup, err := w.buildMail(ctx, mail)
	if err != nil {
		return
	}
	defer cleanup()
	return w.sendMail(mail, email)
}

func (w *Worker) buildMail(ctx context.Context, mail *api.Mail) (email *mailyak.MailYak, cleanup func(), err error) {
	email = mailyak.New("", nil)
	email.From(mail.From)
	if mail.FromName != "" {
		email.FromName(mail.FromName)
	}
	email.To(mail.To)
	email.Subject(mail.Subject)
	if mail.CC != nil {
		email.Cc(mail.CC...)
	}
	if mail.BCC != nil {
		email.Bcc(mail.BCC...)
	}
	if mail.ReplyTo != "" {
		email.ReplyTo(mail.ReplyTo)
	}
	_, _ = email.HTML().WriteString(mail.Message)

	readers := make([]io.Closer, 0, len(mail.Attachments))
	cleanup = func() {
		for _, reader := range readers {
			_ = reader.Close()
		}
	}

	for _, attachment := range mail.Attachments {
		content, err := w.Loader.Load(ctx, attachment.Url)
		if err != nil {
			cleanup()
			return nil, nil, err
		}
		if attachment.Inline {
			email.AttachInlineWithMimeType(attachment.Name, content, attachment.Type)
		} else {
			email.AttachWithMimeType(attachment.Name, content, attachment.Type)
		}
		readers = append(readers, content)
	}

	return
}

func (w *Worker) sendMail(mail *api.Mail, email *mailyak.MailYak) (err error) {
	content, err := email.MimeBuf()
	if err != nil {
		return
	}
	buffer := w.BufferPool.Get().(buffer)
	buffer.Reset()
	defer w.BufferPool.Put(buffer)
	if err = w.Signer.Sign(buffer, content); err != nil {
		return
	}
	receivers := []string{mail.To}
	if mail.CC != nil {
		receivers = append(receivers, mail.CC...)
	}
	if mail.BCC != nil {
		receivers = append(receivers, mail.BCC...)
	}
	return smtp.SendMail(w.Addr, w.Auth, mail.From, receivers, buffer.Bytes())
}
