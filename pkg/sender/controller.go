package sender

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/tonyhhyip/mailrifle/pkg/api"
	"gocloud.dev/pubsub"
	"sync"
)

type ErrorLogger interface {
	Error(message string)
}

type Controller struct {
	Subscription *pubsub.Subscription
	ErrorLogger  ErrorLogger
	closeCh      chan struct{}
	workerPool   *sync.Pool
}

func (c *Controller) Init(createWorker func() *Worker) {
	c.closeCh = make(chan struct{})
	c.workerPool = &sync.Pool{
		New: func() interface{} {
			return createWorker()
		},
	}
}

func (c *Controller) Start(maxHandlers int) {
	workload := make(chan struct{}, maxHandlers)
recvLoop:
	for {
		msg, err := c.Subscription.Receive(context.Background())
		if err != nil {
			// Errors from Receive indicate that Receive will no longer succeed.
			c.ErrorLogger.Error(fmt.Sprintf("Receiving message: %v", err))
			break
		}

		select {
		case workload <- struct{}{}:
		case <-c.closeCh:
			break recvLoop
		}

		// Handle the message in a new goroutine.
		go func() {
			defer func() { <-workload }() // Release the semaphore.

			// Do work based on the message, for example:
			worker := c.workerPool.Get().(*Worker)
			defer c.workerPool.Put(worker)
			var mail api.Mail
			if err = json.Unmarshal(msg.Body, &mail); err != nil {
				c.ErrorLogger.Error(fmt.Sprintf("Fail to parse message: %v", err))
				if msg.Nackable() {
					msg.Nack()
				}
				return
			}
			if err = worker.Process(context.Background(), &mail); err != nil {
				c.ErrorLogger.Error(fmt.Sprintf("Fail to send email: %v", err))
				if msg.Nackable() {
					msg.Nack()
				}
				return
			}
			msg.Ack()
		}()
	}
	_ = c.Subscription.Shutdown(context.Background())
}

func (c *Controller) Stop() {
	close(c.closeCh)
}
