package sender

import (
	"io"

	"github.com/emersion/go-msgauth/dkim"
)

type DKIMSigner interface {
	Sign(w io.Writer, r io.Reader) error
}

type NilDKIMSigner struct{}

func (s NilDKIMSigner) Sign(w io.Writer, r io.Reader) (err error) {
	_, err = io.Copy(w, r)
	return
}

type StaticKeySigner struct {
	Options *dkim.SignOptions
}

func (s *StaticKeySigner) Sign(w io.Writer, r io.Reader) (err error) {
	return dkim.Sign(w, r, s.Options)
}
