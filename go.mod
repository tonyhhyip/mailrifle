module gitlab.com/tonyhhyip/mailrifle

go 1.13

require (
	github.com/domodwyer/mailyak v3.1.1+incompatible
	github.com/emersion/go-msgauth v0.4.0
	gocloud.dev v0.18.0
)
